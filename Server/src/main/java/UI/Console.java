package UI;

import Domain.Assign;
import Domain.Problems;
import Domain.Students;
import Service.AssignService;
import Service.ProblemService;
import Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.security.validator.ValidatorException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

@Component
public class Console {
    private static Scanner keyboard = new Scanner(System.in);

    @Autowired
    private StudentService studentService;
    @Autowired
    private ProblemService problemService;
    @Autowired
    private AssignService assignService;

    private void addStudent(String[] args){
        Students student = new Students(args[3], args[4], Integer.parseInt(args[5]));
        student.setId(Long.parseLong(args[2]));

        try {
            this.studentService.addStudent(student);
            System.out.println("Student added successfully");
        } catch (ValidatorException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void addProblem(String[] args)
    {
        Problems problem = new Problems(args[3], args[4]);
        problem.setId(Long.parseLong(args[2]));

        try {
            this.problemService.addProblem(problem);
            System.out.println("Problem added successfully");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void addAssignment(String[] args)
    {
        Assign assignment = new Assign(Long.parseLong(args[3]), Long.parseLong(args[4]), Integer.parseInt(args[5]));
        assignment.setId(Long.parseLong(args[2]));

        try {
            this.assignService.addAssignment(assignment);
            System.out.println("Assignment added succesfully");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void removeStudent(String[] args)
    {
        this.studentService.deleteStudent(Long.parseLong(args[2]));
        System.out.println("Student removed successfully");
    }

    private void removeProblem(String[] args)
    {
        this.problemService.deleteProblem(Long.parseLong(args[2]));
        System.out.println("Problem removed successfully");
    }

    private void removeAssignment(String[] args)
    {
        this.assignService.deleteAssignment(Long.parseLong(args[2]));
        System.out.println("Assignment removed successfully");
    }

    private void updateStudent(String[] args)
    {
        Students student = new Students(args[3], args[4], Integer.parseInt(args[5]));
        student.setId(Long.parseLong(args[2]));

        this.studentService.updateStudent(student);
        System.out.println("Student updated successfully");
    }

    private void updateProblem(String[] args)
    {
        Problems problem = new Problems(args[3], args[4]);
        problem.setId(Long.parseLong(args[2]));

        this.problemService.updateProblem(problem);
        System.out.println("Problem updated successfully");
    }

    private void displayStudents()
    {
        this.studentService.getAll().forEach(System.out::println);
    }

    private void displayProblems()
    {
        this.problemService.getAll().forEach(System.out::println);
    }

    private void displayAssignments()
    {
        this.assignService.getAll().forEach(System.out::println);
    }

    private void filterStudents(String[] args)
    {
        this.studentService.filterStudentsByName(args[2]).forEach(System.out::println);
    }

    private void filterProblems(String[] args)
    {
        this.problemService.filterProblemsByName(args[2]).forEach(System.out::println);
    }

    private void getMostAssigned()
    {
        long response =  this.assignService.getMostAssignedProblem();
        System.out.println(assignService.getByID(response)); //daca nu merge asa, o sa returnam numai valoare din getMostAssignedProblem
    }

    public void run() {menu();}

    private void menu()
    {
        String cmd;
        System.out.println("--- Lab Problems ---");

        while(true)
        {
            System.out.println(">");
            cmd = keyboard.nextLine();

            String[] cmdArgs = Arrays.stream(cmd.split(" ")).toArray(String[]::new);
            switch(cmdArgs[0])
            {

                case "add":
                    switch (cmdArgs[1]) {
                        case "student":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add student [ID] [SerialNumber] [Name] [Group]");
                                continue;
                            } else addStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: add problem [ID] [SerialNumber] [Description]");
                                continue;
                            } else addProblem(cmdArgs);
                            break;
                        case "assignment":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add assignment [ID] [Student_ID] [Problem_ID] [Grade]");
                                continue;
                            } else addAssignment(cmdArgs);
                            break;
                        default:
                            break;
                    }
                case "remove":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove student [ID]");
                                continue;
                            } else removeStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove problem [ID]");
                                continue;
                            } else removeProblem(cmdArgs);
                            break;
                        case "assignment":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove assignment [ID]");
                                continue;
                            } else removeAssignment(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "update":
                    switch (cmdArgs[1])
                    {

                        case "student":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add student [ID] [SerialNumber] [Name] [Group]");
                                continue;
                            } else updateStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: add problem [ID] [SerialNumber] [Description]");
                                continue;
                            } else updateProblem(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "display":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove student");
                                continue;
                            } else displayStudents();
                            break;
                        case "problem":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove problem");
                                continue;
                            } else displayProblems();
                            break;
                        case "assignment":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove assignment");
                                continue;
                            } else displayAssignments();
                            break;
                        default:
                            break;
                    }
                    break;
                case "filter":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: filter student [Name]");
                                continue;
                            } else filterStudents(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: filter problem [Description]");
                                continue;
                            } else filterProblems(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "report":
                    getMostAssigned();
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Invalid command");
            }
        }
    }
}
