package Domain.Validator;

import Domain.Students;
import sun.security.validator.ValidatorException;

public class StudentsValidator implements Validator<Students> {
    @Override
    public void validate(Students entity) throws ValidatorException {
        if(entity.getName()==null || entity.getSerialNumber()==null){
            throw new Domain.Validator.ValidatorException("name and serial number cannot be null");
        }
    }
}
