package Domain.Validator;

import Domain.Problems;
import sun.security.validator.ValidatorException;

public class ProblemsValidator implements Validator<Problems> {
    @Override
    public void validate(Problems entity) throws ValidatorException {
        if(entity.getDescr()==null || entity.getSerialNumber()==null){
            throw new Domain.Validator.ValidatorException("description and serial number cannot be null");
        }
    }
}
