package Domain.Validator;

import sun.security.validator.ValidatorException;

public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
