package Domain.Validator;

import Domain.Assign;
import sun.security.validator.ValidatorException;

public class AssignmentValidator implements Validator<Assign> {
    @Override
    public void validate(Assign entity) throws ValidatorException {
        if (entity.getProblemsid()==null || entity.getStudentsid()==null){
            throw new Domain.Validator.ValidatorException("student or problem does not exist");
        }
    }
}