package Domain;

import javax.persistence.Entity;

@Entity
public class Students extends BaseEntity<Long> {
    public String serialNumber;
    public String name;
    public int sgroup;

    public Students(){

    }

    public Students(String serialNumber, String name, int group) {
        this.serialNumber = serialNumber;
        this.name = name;
        this.sgroup = group;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup() {
        return sgroup;
    }

    public void setGroup(int group) {
        this.sgroup = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Students student = (Students) o;
        return sgroup == student.sgroup &&
                serialNumber.equals(student.serialNumber) &&
                name.equals(student.name);
    }

    @Override
    public int hashCode() {
        int result = serialNumber.hashCode();
        result=31*result+name.hashCode();
        result=result*31+sgroup;
        return result;
    }


    @Override
    public String toString() {
        return "Students{ID: " +super.toString()+", "+
                "serialNumber='" + serialNumber + '\'' +
                ", name='" + name + '\'' +
                ", group=" + sgroup +
                '}';
    }
}
