package Domain;

import javax.persistence.Entity;

@Entity
public class Assign extends BaseEntity<Long> {
    private Long studentsid;
    private Long problemsid;
    private int grade;

    public Long getStudentsid() {
        return studentsid;
    }

    public void setGrade(int grade)
    {
        this.grade = grade;
    }

    public int getGrade()
    {
        return this.grade;
    }

    public void setStudentsid(Long studentsid) {
        this.studentsid = studentsid;
    }

    public Long getProblemsid() {
        return problemsid;
    }

    public void setProblemsid(Long problemsid) {
        this.problemsid = problemsid;
    }

    @Override
    public String toString() {
        return "Assign{ID" +super.toString()+
                "studentsid=" + studentsid +
                ", problemsid=" + problemsid +
                ", grade=" + grade +
                '}';
    }

    public Assign(){

    }

    public Assign(Long studentsid, Long problemsid, int grade) {
        this.studentsid = studentsid;
        this.problemsid = problemsid;
        this.grade = grade;
    }

    public Assign(Long studentsid, Long problemsid) {
        this.studentsid = studentsid;
        this.problemsid = problemsid;
        this.grade = 0;
    }
}