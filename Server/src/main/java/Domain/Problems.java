package Domain;

import Service.ProblemService;

import javax.persistence.Entity;

@Entity
public class Problems extends BaseEntity<Long> {
    private String serialNumber;
    private String descr;

    public Problems(){

    }

    public Problems(String serialNumber, String descr) {
        this.serialNumber = serialNumber;
        this.descr = descr;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Problems problems = (Problems) o;
        return serialNumber.equals(problems.serialNumber) &&
                descr.equals(problems.descr);
    }

    @Override
    public int hashCode() {
        int result = serialNumber.hashCode();
        result=31*result+descr.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "Problems{ID: " +super.toString()+", "+
                "serialNumber='" + serialNumber + '\'' +
                ", descr='" + descr + '\'' +
                '}';
    }
}
