package Service;

import Domain.Problems;
import Domain.Validator.ValidatorException;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public interface ProblemService extends Service<Long, Problems> {
    void addProblem(Problems p) throws ValidatorException, IOException;
    Set<Problems> filterProblemsByName(String s);
    void deleteProblem(Long id);
    void updateProblem(Problems p);
}
