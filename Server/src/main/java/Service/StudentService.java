package Service;

import Domain.Students;
import sun.security.validator.ValidatorException;

import java.io.IOException;
import java.util.Set;

public interface StudentService extends Service<Long, Students> {
    void addStudent(Students student) throws ValidatorException, IOException;
    Set<Students> filterStudentsByName(String s);
    void deleteStudent(Long id);
    void updateStudent(Students s);
}
