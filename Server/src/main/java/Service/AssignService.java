package Service;

import Domain.Assign;
import Domain.Validator.ValidatorException;

import java.io.IOException;

public interface AssignService extends Service<Long, Assign> {
    void addAssignment(Assign assign) throws ValidatorException, IOException;
    long getMostAssignedProblem();
    void deleteAssignment(Long id);

}
