package Service;


import Domain.Students;
import Repository.StudentRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component("studentService")
public class StudentServiceImpl implements StudentService {
    public static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentRepo studentRepo;

    @Override
    public void addStudent(Students student) {
        log.trace("addStudent - method entered");
        studentRepo.save(student);
        log.trace("addStudent - method finished");
    }

    @Override
    public Set<Students> getAll()
    {       log.trace("getAll - method entered");
            Iterable<Students> students = studentRepo.findAll();
            log.trace("getAll - method finished");
            return StreamSupport.stream(students.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public Set<Students> filterStudentsByName(String name)
    {
        log.trace("filterStudentsByName - method entered");
        Iterable<Students> students = studentRepo.findAll();

        Set<Students> filtered=new HashSet<>();
        students.forEach(filtered::add);
        filtered.removeIf(stu->!stu.getName().contains(name));
        log.trace("filterStudentsByName - method finished");
        return filtered;
    }

    @Override
    public void deleteStudent(Long id)
    {
        log.trace("deleteStudent - method entered");
       studentRepo.deleteById(id);
        log.trace("deleteStudent - method finished");
    }

    @Override
    @Transactional
    public void updateStudent(Students student)
    {
        log.trace("updateStudent - method entered");
        studentRepo.findById(student.getId()).ifPresent(s -> {
            s.setSerialNumber(student.getSerialNumber());
            s.setName(student.getName());
            s.setGroup(student.getGroup());
        });
        log.trace("updateStudent - method finished");
    }

    @Override
    public Students getByID(Long id)
    {
        log.trace("getByID - method entered");
        Optional<Students> student = studentRepo.findById(id);
        log.trace("getByID - method finished");
        if(student.isPresent())
            return student.get();
        else throw new RuntimeException("Couldn't find the student with the given id");
    }
}
