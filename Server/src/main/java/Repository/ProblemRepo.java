package Repository;

import Domain.Problems;

public interface ProblemRepo extends Repository<Long, Problems> {
}
