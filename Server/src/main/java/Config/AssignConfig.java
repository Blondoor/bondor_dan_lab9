package Config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"Repository", "Service", "Domain", "UI"})
public class AssignConfig {
}
